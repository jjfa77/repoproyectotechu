var express = require('express'),
  app = express(),
  port = process.env.PORT || 2000;

//variable para poder usar el request-json
var requestJson = require('request-json');
//Variable para que desde local ejecute un contenedor
var cors = require('cors');
//variable para que se pueda parsear el req.body y no salga como undefined
var bodyParser = require('body-parser');

var path = require('path');

app.use(bodyParser.json()); // for parsing application/json
app.use(cors());

//Para mostrar en el arranque el puerto que se levanta
app.listen(port);
console.log('todo list RESTful API server started on: ' + port);

/////////////////////
////// POSTGRE //////
/////////////////////
var pg = require('pg');
//Si me conecto al local var urlUsuarios = "postgres://localhost:5433"  para docker --> bacpostgre:5432
var urlBBDD = "postgres://docker:docker@bacpostgre:5432/bdlogin";
//var urlBBDD = "postgres://docker:docker@localhost:5433/bdlogin";

////////////////////////////////////////////////
/// VALIDAR SI EL USUARIO ACCEDE EN EL LOGIN ///
////////////////////////////////////////////////
app.post('/login', function(req, res){

  console.log("ENTRO EN LA COMPROBACIÓN DEL LOGIN");

  var clientePosgre = new pg.Client(urlBBDD);
  clientePosgre.connect();
  const query = clientePosgre.query('SELECT * FROM tbusuarios WHERE dni=$1 AND password=$2;', [req.body.dni, req.body.password], (err, result) => {

    if(err){
      console.log(err);
      res.sendStatus(403);
    }else{
      if (result.rowCount == '0')      res.sendStatus(204);//res.send("KO");
      else                             res.status(200).send(result.rows[0].nombre + " " + result.rows[0].apellido1);//res.send("OK");
//      clientePosgre.end();
    }
  });
});

////////////////////////////////////////////
/// BUSCAR TODOS LOS DATOS DE UN USUARIO ///
////////////////////////////////////////////
app.get('/usuarios/:dni', function(req, res){

  console.log("ENTRO EN LA BÚSQUEDA DE TODOS LOS DATOS DEL USUARIO");

  var clientePosgre = new pg.Client(urlBBDD);
  clientePosgre.connect();
  const query = clientePosgre.query('SELECT * FROM tbusuarios WHERE dni=$1;', [req.params.dni], (err, result) => {

    if(err){
      console.log(err);
      res.sendStatus(403);
    }else{
      if (result.rowCount == '0')      res.sendStatus(204);//res.send("KO");
      else                             res.status(200).send(result.rows[0]);//res.send("OK");
//      clientePosgre.end();
    }
  });
});

////////////////////////////////////////////
/// COMPROBAR QUE EL SECRETO ES CORRECTO ///
////////////////////////////////////////////
app.post('/secreto', function(req, res){

  console.log("ENTRO EN LA COMPROBACIÓN QUE EL SECRETO ES CORRECTO");

  var clientePosgre = new pg.Client(urlBBDD);
  clientePosgre.connect();
  const query = clientePosgre.query('SELECT * FROM tbusuarios WHERE dni=$1 AND secreto=$2;', [req.body.dni, req.body.secreto], (err, result) => {

    if(err){
      console.log(err);
      res.sendStatus(403);
    }else{
      if (result.rowCount == '0')      res.sendStatus(204);//res.send("KO");
      else                             res.status(200).send(result.rows[0].nombre);//res.send("OK");
//      clientePosgre.end();
    }
  });
});

///////////////////////////
/// CAMBIAR LA PASSWORD ///
///////////////////////////
app.patch('/usuarios/:dni', function(req, res){

  console.log("ENTRO EN EL CAMBIO DE LA PASSWORD DEL USUARIO");

  var clientePosgre = new pg.Client(urlBBDD);
  clientePosgre.connect();
  const query = clientePosgre.query('UPDATE tbusuarios SET password=$2 WHERE dni=$1;', [req.params.dni, req.body.password], (err, result) => {

    if(err){
      console.log(err);
      res.sendStatus(403);
    }else{
      if (result.rowCount == '0')      res.sendStatus(400);//res.send("KO");
      else                             res.sendStatus(201);//res.send("OK"");
//      clientePosgre.end();
    }
  });
});

///////////////////////////////////////
/// ACTUALIZA UN USUARIO EN POSGRES ///
///////////////////////////////////////
app.put('/usuarios/:dni', function(req, res){

  console.log("ENTRO EN LA ACTUALIZACIÓN DE DATOS DE UN CLIENTE");

  var clientePosgre = new pg.Client(urlBBDD);
  clientePosgre.connect();
  const query = clientePosgre.query('UPDATE tbusuarios SET password=$2, nombre=$3, apellido1=$4, apellido2=$5, fechanacimiento=$6, sexo=$7, email=$8, pregunta=$9, secreto=$10 WHERE dni=$1;',
                [req.params.dni, req.body.password, req.body.nombre, req.body.apellido1, req.body.apellido2,
                req.body.fecNacimiento, req.body.sexo, req.body.email, req.body.preguntaSecreta, req.body.secreto], (err, result) => {

    if(err){
      console.log(err);
      res.sendStatus(403);
    }else{
      if (result.rowCount == '0')      res.sendStatus(400);//res.send("KO");
      else                             res.sendStatus(201);//res.send("OK"");
//      clientePosgre.end();
    }
  });
});

////////////////////////////////////////
/// DA DE ALTA UN USUARIO EN POSGRES ///
////////////////////////////////////////
app.post('/usuarios', function(req, res){

  console.log("ENTRO EN EL ALTA DE UN USUARIO EN POSGRES");

  var clientePosgre = new pg.Client(urlBBDD);
  clientePosgre.connect();
  const query = clientePosgre.query('INSERT INTO tbusuarios values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10);',
                [req.body.dni, req.body.password, req.body.nombre, req.body.apellido1, req.body.apellido2,
                 req.body.fecNacimiento, req.body.sexo, req.body.email, req.body.preguntaSecreta, req.body.secreto], (err, result) => {

    if(err){
      console.log(err);
      res.sendStatus(403);
    }else{
      res.sendStatus(200);//res.send("OK");
    }
//    clientePosgre.end();
  });
});

////////////////////////////////////////
/// DA DE BAJA UN USUARIO EN POSGRES ///
////////////////////////////////////////
app.delete('/usuarios/:dni', function(req, res){

  console.log("ENTRO EN EL BAJA DE UN USUARIO");

  var clientePosgre = new pg.Client(urlBBDD);
  clientePosgre.connect();
  const query = clientePosgre.query('DELETE FROM tbusuarios WHERE dni=$1;', [req.params.dni], (err, result) => {

    if(err){
      console.log(err);
      res.sendStatus(403);
    }else{
      res.sendStatus(200);//res.send("OK");
    }
//    clientePosgre.end();
  });
});

////////////////////
/// MONGO - MLAB ///
////////////////////
const mongoClient = require('mongodb').MongoClient;
const urlMongo = 'mongodb://jjfa:jj4450@ds161316.mlab.com:61316/bdmovimientos';

//////////////////////////////////////////////
/// DA DE ALTA UN USUARIO + CUENTA EN MLAB ///
//////////////////////////////////////////////
app.post('/movimientos', function(req, res) {

  console.log("ENTRO EN EL ALTA DE UN USUARIO Y CUENTA");

  //ESTABLEZCO CONEXION
  mongoClient.connect(urlMongo, (err, db) => {

    if(err){
      console.log(err);
      res.send(err);
    }else{
      db.collection('movimientos').insertOne(req.body, function (err, res) {
        if (err) return console.log(err)
        db.close();
      });

      if (err)    res.sendStatus(400);//res.send("KO");
      else        res.sendStatus(200);//res.send("OK");
    }
  });
});

//////////////////////////////////////////////
/// DA DE BAJA UN USUARIO + CUENTA EN MLAB ///
//////////////////////////////////////////////
app.delete('/movimientos/:dni', function(req, res) {

  console.log("ENTRO EN LA ELIMINACIÓN DE UN MOVIMIENTO");
  var dni = {"_id": parseInt(req.params.dni)};

  //ESTABLEZCO CONEXION
  mongoClient.connect(urlMongo, (err, db) => {

    if(err){
      console.log(err);
      res.send(err);
    }else{
      db.collection('movimientos').deleteOne(dni, function (err, res) {
        if (err) return console.log(err)
        db.close();
      });

      if (err)    res.sendStatus(400);//res.send("KO");
      else        res.sendStatus(200);//res.send("OK");
    }
  });
});

///////////////////////////////////////////////
/// AÑADE UNA CUENTA A UN DOCUMENTO EN MLAB ///
//////////////////////////////////////////////
app.put('/movimientos/:dni', function(req, res) {

  console.log("ENTRO EN EL ALTA DE UN MOVIMIENTO");

  //ESTABLEZCO CONEXION
  mongoClient.connect(urlMongo, (err, db) => {
    var query = {"_id": parseInt(req.params.dni)};

    if(err){
      console.log(err);
      res.send(err);
    }else{
      db.collection('movimientos').updateOne(query, req.body, function (err, res) {
        if (err) return console.log(err)
        db.close();
      });

      if (err)    res.sendStatus(400);//res.send("KO");
      else        res.sendStatus(200);//res.send("OK");
    }
  });
});

//////////////////////////////////////////////////
/// CONSULTA LAS CUENTAS DE UN USUARIO EN MLAB ///
//////////////////////////////////////////////////
app.get('/movimientos/:dni', function(req, res) {

  console.log("ENTRO EN LA CONSULTA DE CUENTAS");

  var idCliente = parseInt(req.params.dni);
  var urlCuentasCliente = "https://api.mlab.com/api/1/databases/bdmovimientos/collections/movimientos?apiKey=x4tbXft2sn3rC-Ea1xl_d5Zpt4SYoa3W&q={_id:" + idCliente + "}";

    var clienteMlab = requestJson.createClient(urlCuentasCliente);
    clienteMlab.get('', function(err, resM, body) {

    if (err)    res.sendStatus(400);//res.send("KO");
    else        res.status(200).send(body);//res.send("OK");
  });
});

//////////////////////////////////////////////////////////////
/// CONSULTA LOS MOVIMIENTOS DE UNA CUENTA-USUARIO EN MLAB ///
//////////////////////////////////////////////////////////////
app.get('/usuarios/:dni/cuentas/:idCuenta', function(req, res) {

  console.log("ENTRO EN LA CONSULTA DE MOVIMIENTOS");

  var idCliente = parseInt(req.params.dni);
  var idCuenta = parseInt(req.params.idCuenta);
  var urlMovimientosCliente = "https://api.mlab.com/api/1/databases/bdmovimientos/collections/movimientos?apiKey=x4tbXft2sn3rC-Ea1xl_d5Zpt4SYoa3W&q={_id:" + idCliente + ",cuentas.idCuenta:" + idCuenta + "}&f={_id:0,cuentas:0,cuentas.movimientos:1,cuentas:{$elemMatch:{idCuenta:" + idCuenta + "}}}";

    var clienteMlab = requestJson.createClient(urlMovimientosCliente);
    clienteMlab.get('', function(err, resM, body) {

      if (err)    res.sendStatus(400);//res.send("KO");
      else        res.status(200).send(body);//res.send("OK");
  });
});

///////////////////////////////////////////////
/// ACTUALIZA UN DATO DEL DOCUMENTO (SALDO) ///
///////////////////////////////////////////////
app.patch('/usuarios/:dni/cuentas/:idCuenta', function(req, res) {

  console.log("ENTRO EN LA EDICIÓN DEL SALDO");

  var idCliente = parseInt(req.params.dni);
  var idCuenta = parseInt(req.params.idCuenta);
  var newSaldo = req.body.saldo;

  var query = {"_id": idCliente, "cuentas.idCuenta": parseInt(idCuenta)};
  var cambio = {$set: {"cuentas.$.saldo": newSaldo} };

  console.log(idCliente + " " + idCuenta + " " + newSaldo);

  //ESTABLEZCO CONEXION
  mongoClient.connect(urlMongo, (err, db) => {

    if(err){
      console.log(err);
      res.send(err);
    }else{
      db.collection('movimientos').updateOne(query, cambio, function (err, res) {
        if (err) return console.log(err)
        db.close();
      });

      if (err)    res.sendStatus(400);//res.send("KO");
      else        res.sendStatus(200);//res.send("OK");
    }
  });
});
